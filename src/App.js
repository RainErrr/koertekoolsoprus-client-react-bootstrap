import React from 'react'
import { Switch, Route } from 'react-router-dom'

import Navigation from './components/Navbar'
import Footer from './components/Footer'
import Home from './components/pages/Home'
import DynamicPage from './components/DynamicPage'
import Blog from './components/pages/Blog'
import Admin from './components/pages/Admin'
import AdminTable from './components/AdminTable'
import AdminVideos from './components/AdminVideos'

import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css'

const App = () => {
	return (
		<div>
			<Navigation />
			<Switch>
				<Route exact path='/'>
					<Home />
				</Route>
				<Route path='/article/:dynamicId'>
					<DynamicPage />
				</Route>
				<Route path='/blog'>
					<Blog />
				</Route>
				<Route path='/blogPost/:dynamicId'>
					<DynamicPage />
				</Route>
				<Route path='/admin'>
					<Admin />
					<AdminTable />
				</Route>
				<Route path='/adminVideos'>
					<Admin />
					<AdminVideos />
				</Route>
			</Switch>
			<Footer />
		</div>
	)
}
export default App
