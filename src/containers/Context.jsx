import React, { useState, useEffect } from 'react'

const Context = React.createContext()

const ContextProvider = ({ children }) => {
	const [allArticles, setAllArticles] = useState([])

	const url = 'http://localhost:8080/articles/active'

	useEffect(() => {
		fetch(url)
			.then((response) => response.json())
			.then((data) => setAllArticles(data))
	}, [])

	return (
		<Context.Provider value={{ allArticles }}>{children}</Context.Provider>
	)
}

export { ContextProvider, Context }
