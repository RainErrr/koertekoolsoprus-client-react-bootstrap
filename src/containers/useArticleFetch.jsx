import { useEffect, useState } from 'react'

const useArticleFetch = (props) => {
	const [data, setData] = useState([])
	const [isLoading, setIsLoading] = useState(false)
	const [error, setError] = useState(null)

	useEffect(() => {
		setIsLoading(true)
		fetch('http://localhost:8080/articles/' + props)
			.then((response) => {
				if (response.ok) {
					return response.json()
				} else {
					throw Error('Error fetching data!')
				}
			})
			.then((data) => {
				setData(data)
				setIsLoading(false)
			})
			.catch((error) => {
				setError(error)
			})
	}, [props])

	return { data, isLoading, error }
}
export default useArticleFetch
