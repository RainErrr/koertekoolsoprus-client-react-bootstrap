import { useEffect, useState } from 'react'

const useArticlesFetch = () => {
	const [data, setData] = useState([])
	const [isLoading, setIsLoading] = useState(false)
	const [error, setError] = useState(null)

	useEffect(() => {
		setIsLoading(true)
		fetch('http://localhost:8080/articles/active')
			.then((response) => {
				if (response.ok) {
					return response.json()
				} else {
					throw Error('Error fetching Carousel data!')
				}
			})
			.then((data) => {
				setData(data)
				setIsLoading(false)
			})
			.catch((error) => {
				setError(error)
			})
	}, [])

	return { data, isLoading, error }
}

export default useArticlesFetch
