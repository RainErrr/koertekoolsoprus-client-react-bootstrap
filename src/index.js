import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router } from 'react-router-dom'

import './index.css'
import App from './App'
import * as serviceWorker from './serviceWorker'
import { ContextProvider } from './containers/Context'

ReactDOM.render(
	<ContextProvider>
		<React.StrictMode>
			<Router>
				<App />
			</Router>
		</React.StrictMode>
	</ContextProvider>,
	document.getElementById('root')
)

serviceWorker.unregister()
