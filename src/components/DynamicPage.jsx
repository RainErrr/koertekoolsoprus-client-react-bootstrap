import React from 'react'
import { useParams } from 'react-router-dom'

import useArticleFetch from '../containers/useArticleFetch'
import '../css/DynamicPage.css'

const DynamicPage = () => {
	const { dynamicId } = useParams()
	const { data } = useArticleFetch(parseInt(dynamicId))
	const img = data.files

	return (
		<div>
			<div className='topbanner'>
				<img
					src={img ? data.files[0].targetLocation : ''}
					alt={data.heading}
				/>
			</div>
			<div className='container'>
				<div className='text'>
					<h2>{data.heading}</h2>
					<p>{data.body}</p>
				</div>
			</div>
		</div>
	)
}

export default DynamicPage
