import React, { useState, useContext, memo } from 'react'
import { Link } from 'react-router-dom'

import { Context } from '../containers/Context'
import Carousel from 'react-bootstrap/Carousel'

import '../css/Carousel.css'

const RoundAbout = () => {
	const [index, setIndex] = useState(0)

	const handleSelect = (selectedIndex, e) => {
		setIndex(selectedIndex)
	}

	const { allArticles } = useContext(Context)

	return (
		<Carousel activeIndex={index} onSelect={handleSelect}>
			{allArticles
				.filter((data) => data.focusId > 0)
				.map((article) => (
					<Carousel.Item key={article.id}>
						<img
							className='d-block w-100'
							src={article.files[0].targetLocation}
							alt={''}
						/>
						<Carousel.Caption>
							<Link to={`/${article.id}`}>
								<h3>{article.heading}</h3>
							</Link>
							<p>{article.intro}</p>
						</Carousel.Caption>
					</Carousel.Item>
				))}
		</Carousel>
	)
}

export default memo(RoundAbout)
