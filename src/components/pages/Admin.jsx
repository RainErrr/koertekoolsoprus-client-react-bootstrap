import React from 'react'
import { Link } from 'react-router-dom'

import { Nav } from 'react-bootstrap'
import '../../css/Admin.css'

const Admin = () => {
	return (
		<Nav variant='tabs' className='justify-content-end'>
			<Nav.Item>
				<Nav.Link as={Link} to='/admin'>
					Artiklid
				</Nav.Link>
			</Nav.Item>
			<Nav.Item>
				<Nav.Link as={Link} to='/adminVideos'>
					Videod
				</Nav.Link>
			</Nav.Item>
		</Nav>
	)
}

export default Admin
