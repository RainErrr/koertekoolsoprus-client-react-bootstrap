import React from 'react'
import RoundAbout from '../RoundAbout'
import AboutUs from '../AboutUs'
import Trainings from '../Trainings'
import '../../css/Home.css'

const Home = () => {
	return (
		<div>
			<RoundAbout />
			<AboutUs />
			<Trainings />
		</div>
	)
}

export default Home
