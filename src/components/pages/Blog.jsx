import React from 'react'
import { Link } from 'react-router-dom'

import { Card, CardColumns, Container } from 'react-bootstrap'
import useArticlesFetch from '../../containers/useArticlesFetch'
import '../../css/Blog.css'

const Blog = () => {
	const { data } = useArticlesFetch()
	console.table(data)

	return (
		<Container>
			<CardColumns>
				{data
					.filter(
						(blogPost) => blogPost.type === 1 || blogPost.type === 2
					)
					.map((blogPost) => (
						<Card
							id='blog'
							key={blogPost.id}
							as={Link}
							to={`/blogPost/${blogPost.id}`}
						>
							<Card.Img
								className='trainingImg'
								variant='top'
								src={blogPost.files[0].targetLocation}
							/>
							<Card.Body>
								<Card.Title>{blogPost.heading}</Card.Title>
								<Card.Text>{blogPost.intro}</Card.Text>
							</Card.Body>
						</Card>
					))}
			</CardColumns>
		</Container>
	)
}

export default Blog
