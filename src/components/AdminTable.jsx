import React from 'react'

import Table from 'react-bootstrap/Table'
import useArticlesFetch from '../containers/useArticlesFetch'

const AdminTable = () => {
	const { data } = useArticlesFetch()
	return (
		<Table bordered hover className='container'>
			<thead>
				<tr>
					<th>#</th>
					<th>Pealkiri</th>
					<th>Sissejuhatus</th>
					<th>Sisu</th>
					<th>Tüüp</th>
					<th>Fookus</th>
					<th>Aktiivne</th>
				</tr>
			</thead>
			<tbody>
				{data.map((article) => (
					<tr key={article.id}>
						<td>{article.id}</td>
						<td>{article.heading}</td>
						<td>{article.intro}</td>
						<th>{article.body}</th>
						<th>{article.type}</th>
						<th>{article.focusId}</th>
						<td>{article.active ? 'Jah' : 'Ei'}</td>
					</tr>
				))}
			</tbody>
		</Table>
	)
}

export default AdminTable
