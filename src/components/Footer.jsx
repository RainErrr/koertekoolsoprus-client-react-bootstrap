import React from 'react'
import '../css/Footer.css'
import logo2hall from '../images/logo2hall.png'

const Footer = () => {
	return (
		<footer>
			<div className='row justify-content-center'>
				<div className='col-md-5 text-center'>
					<img src={logo2hall} alt='' />
					<p>
						Meie koolis käib õppimine peamiselt läbi mängu ehk nagu
						öeldakse „mängeldes läbi elu“! Koerad õpivad mängides
						suurima hea meelega uusi oskusi nii igapäeva eluks, kui
						ka erinevate spordialade tarbeks. Ja kui mängimine
						koertele tegelikult meeldib, siis miks mitte seda
						kasutada?! Kui soovid oma koeraga lõbusasti aega veeta
						ja samas õppida uusi koosveetmise võimalusi, ootame Sind
						ja Sinu lemmikut trenni!
					</p>
					<strong>Kontakt</strong>
					<p>
						+372 55602575, +372 5571160
						<br />
						koertekoolsoprus@gmail.com
					</p>

					<a href='https://www.facebook.com/KoertekoolSoprus/'>
						<i className='fab fa-facebook-square'></i>
					</a>
					<a href='https://www.instagram.com/koertekoolsprus/'>
						<i className='fab fa-instagram'></i>
					</a>
				</div>
				<hr className='socket' />
				&copy; Koertekool Sõprus.
			</div>
		</footer>
	)
}

export default Footer
