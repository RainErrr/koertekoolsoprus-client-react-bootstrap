import React from 'react'
import { Link } from 'react-router-dom'

import { Navbar, Nav } from 'react-bootstrap'
import logo2valge from '../images/logo2valge.png'
import '../css/Navbar.css'

const Navigation = () => {
	return (
		<Navbar collapseOnSelect expand='lg' bg='light' variant='light'>
			<Navbar.Brand as={Link} to='/'>
				<img
					src={logo2valge}
					className='d-inline-block align-top'
					alt='React Bootstrap logo'
				/>
			</Navbar.Brand>
			<Navbar.Toggle aria-controls='responsive-navbar-nav' />
			<Navbar.Collapse id='responsive-navbar-nav'>
				<Nav className='ml-auto'>
					<Nav.Link as={Link} to='/admin'>
						Admin
					</Nav.Link>
					<Nav.Link as={Link} to='#Trainings'>
						Treeningud
					</Nav.Link>
					<Nav.Link as={Link} to='/blog'>
						Blogi
					</Nav.Link>
					<Nav.Link as={Link} to='#Contacts'>
						Kontakt
					</Nav.Link>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}

export default Navigation
