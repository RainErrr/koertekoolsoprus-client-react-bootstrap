import React, { useContext } from 'react'

import Card from 'react-bootstrap/Card'
import CardDeck from 'react-bootstrap/CardDeck'

import { Context } from '../containers/Context'
import '../css/Trainings.css'

const Trainings = () => {
	const { allArticles } = useContext(Context)

	return (
		<div>
			<h1 className='heading'>Treeningud</h1>
			<CardDeck>
				{allArticles
					.filter((article) => article.type === 2)
					.map((trainingArticle) => (
						<Card key={trainingArticle.id}>
							<Card.Img
								className='trainingImg'
								variant='top'
								src={trainingArticle.files[0].targetLocation}
							/>
							<Card.Body>
								<Card.Title>
									{trainingArticle.heading}
								</Card.Title>
								<Card.Text>{trainingArticle.body}</Card.Text>
							</Card.Body>
							<Card.Footer></Card.Footer>
						</Card>
					))}
			</CardDeck>
		</div>
	)
}

export default Trainings
