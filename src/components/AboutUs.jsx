import React from 'react'
import '../css/AboutUs.css'
import katariina from '../images/katariina.jpg'
import julia from '../images/julia.jpeg'
import Card from 'react-bootstrap/Card'
import CardDeck from 'react-bootstrap/CardDeck'

const AboutUs = () => {
	return (
		<div>
			<h1 className='heading'>Meist</h1>
			<CardDeck>
				<Card>
					<Card.Img variant='top' src={katariina} />
					<Card.Body>
						<Card.Title>Katariina Menge</Card.Title>
						<Card.Text>
							Esimene suurem huvi koerte treeningu vastu tekkis
							aastal 2005 kui olin 9.a vana ning sain enda esimese
							koera, kellega teekonda Koertekool
							Tubli kutsikakoolis ning hiljem jätkasime
							sõnakuulelikuse ja agility trennis. Koerte
							treenimine on mulle nende aastate jooksul meeletult
							huvi pakkunud, mille tõttu sai osaletud paljudel
							koolitustel ja võistlustel. Olen lõpetanud Absolute
							Dogs'i koolituse, Pro Dog Treeneriks ja oma
							treeningutes kasutan mängupõhist konseptsiooni ehk
							eluks vajalike oskusi õpetatakse läbi mängu. Minu
							jaoks on tähtis koera mõista ja seda õpetan ka
							teistele!
						</Card.Text>
					</Card.Body>
				</Card>
				<Card>
					<Card.Img variant='top' src={julia} />
					<Card.Body>
						<Card.Title>Julia Plamus</Card.Title>
						<Card.Text>
							Koerte koolitamisega olen tegelenud viimased viis
							aastat ja peamiselt koertega, kelle käitumises on
							mõni selline nüanss juures, mis ei ole kooskõlas
							omaniku soovidega. Olgu see siis probleem või mure
							või ka lihtsalt kerge kiiks, lahenduse olen püüdnud
							leida igal juhul lähtuvalt koerast, konkreetsest
							olukorras ja koera omanikust. Olen ikka seda meelt,
							et see mis sobib ühele, ei pruugi sobida teisele
							ning sellest põhimõttest lähtun ka oma trennides.
							Lisaks usun, et koeri on võimalik koolitada hea sõna
							ning preemiaga ja läbi teineteise mõistmise. Hea ja
							tugev suhe koeraga on see, mis määrab meie
							õnnestumised treeningutes.
						</Card.Text>
					</Card.Body>
				</Card>
			</CardDeck>
		</div>
	)
}

export default AboutUs
